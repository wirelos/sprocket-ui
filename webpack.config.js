module.exports = {
    entry: './src/app/main.js',
    output: {
        filename: './data/script.js'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            loader: 'babel-loader',
            query: {
                presets: ['env'],
                plugins: ['transform-runtime']
            }
        }],
        rules: [{
            test: /\.html$/,
            use: 'raw-loader'
        }]
    },
    resolve: {
        extensions: ['.js', '.json']
    }
};