export default class WsMediator {

  // FIXME mediator should manage all endpoints?
  constructor(endpoint) {
    this.events = [];
    this.endpoint = endpoint;
    this.ws = new WebSocket("ws://" + this.endpoint);
    this.ws.onopen = (event) => {
        console.log('init WsMediator');
    };
    this.ws.onmessage = (event) => {
        console.log(event.data);
        this.trigger(this.endpoint, event.data);
    };
  }

  on(event, callback, context){
    this.events[event] = this.events[event] || [];
    this.events[event].push(context ? callback.bind(context) : callback);
  };

  trigger(event, args){
    if(this.events[event]){
      for (var i = this.events[event].length - 1; i >= 0; i--) {
        this.ws.send(this.events[event][i](args || {}));
      };
    }
  };

}
