import Mustache from 'mustache';
import $ from 'jquery';
import DataField from './data/DataField';


export default class Component {

    constructor(ctx, node, template) {
        this.ctx = ctx;
        this.node = node ? node : $();
        this.component = this.constructor.name;
        this.config = this.node.data() || {name: 'none'};
        this.data = {};
        this.markup = template;
    }

    beforeInit() {
        this.subscribe();
    }

    init() { }
    subscribe() { }
    templateHelpers() { return {}; }

    bindData() {
        let _this = this;
        this.node.find('[data-bind]').each(function () {
            var field = $(this);
            _this.data[field.data('bind')] = new DataField(field);
        });
    }

    render(data) {
        if (data) data.helpers = this.templateHelpers();
        let rendered = Mustache.render(this.markup, data);
        if(!this.node) this.node = $(rendered);
        this.node.html(rendered);
        this.bindData();
    }

}