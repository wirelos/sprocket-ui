
import $ from 'jquery';
import Select from './base/Select/Select';

export default class ParamSelect extends Select {

    constructor(ctx, node) {
        super(ctx, node);
        let viewData = {
            label: this.config.label,
            name: this.config.name
        };
        if(this.config.external){
            $.getJSON(this.config.external).done((data) => {
                viewData.entries = data;
                this.render(viewData);
            })
        } else {
            viewData.entries = this.config.entries;
            this.render(viewData);
        }
        
        this.selectEntry(this.config.default);
        this.ctx.mediator.on(this.config.topic, (payload) => {
            this.selectEntry(payload);
        });
    }

    selectEntry(payload){
        this.node.find('option[value="'+payload+'"]').attr('selected', 'selected');
    }

    onChange(evt) {
        let obj = {
            topic: this.config.topic,
            payload: evt.target.value,
            broadcast: 1
        };
        this.ctx.mediator.trigger(obj.topic, obj.payload);
        this.ctx.ws.send(JSON.stringify(obj));

    }

}