import $ from 'jquery';
import TextInput from './base/TextInput/TextInput';
import Store from '../core/store/RestStore';

export default class ParamWs extends TextInput {

    constructor(ctx, node) {
        super(ctx, node);
        //this.store = new Store(this.config.endpoint);

        /* this.ws = new WebSocket(this.config.endpoint.indexOf('/') === 0 ? "ws://" + window.location.host + this.config.endpoint : this.config.endpoint );
        this.ws.onopen = (event) => {
            console.log('open ' + this.config.endpoint);
        };
        this.ws.onmessage = (event) => {
            //console.log(event.data);
            this.node.find('input').val(event.data);
        }; */

        this.ctx.mediator.on(this.config.endpoint, this.onMessage.bind(this));
    }

    onMessage(msg) {
        //console.log('onMsg: ' + msg);
        //this.node.val(msg);
    }

    onInput(evt) {
        let obj = {};
        obj[this.config.name] = this.value;
        //this.store.save(obj);
        console.log(this.value);
        this.ctx.ws.send(JSON.stringify({
            topic: this.config.topic,
            payload: this.value
        }));
        this.ctx.mediator.trigger(this.config.endpoint, this.value);
    }

}