import $ from 'jquery';
import Switch from './base/Switch/Switch';
import RestStore from '../core/store/RestStore';

export default class LedStripPatternSwitch extends Switch {

    constructor(ctx, node) {
        super(ctx, node);
        //this.store = new RestStore(this.config.endpoint);

        this.ws = this.ctx.ws;

        this.modes = ["init", "Color", "Pattern", "octoPrint"];
        this.patterns = [ "none", "Rainbow", "Scanner", "ColorWipe", "TheaterChase"];
    }

    onClick(evt) {
        // FIXME separate mode from pattern
        let payload = {
            //id: this.patterns.indexOf(this.config.name),
            //pattern: this.config.name.substring(0,1),
            //group: this.config.group,
            //mode: 'P',
            //state: this.state
            value: ''+this.config.id,
            mode: ''+this.config.mode,
            foo: this.value
        };
        this.ctx.mediator.trigger('/ledStripPreset/switched', payload);
    }

    subscribe() {
        this.ctx.mediator.on('/ledStripPreset/switched', (payload) => {
            //this.store.save(payload);
            console.log(payload);
            this.ws.send(JSON.stringify(payload));
            if (payload.state
                && payload.id !== this.patterns.indexOf(this.config.name)
                && payload.group === this.config.group) {
                if (this.state) {
                    this.state = false;
                    this.node.find('input').click();
                }
            }
        });
    }

}