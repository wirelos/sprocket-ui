import $ from 'jquery';
import Switch from './base/Switch/Switch';
import Store from '../core/store/RestStore';

export default class PresetSwitch extends Switch {

    constructor(ctx, node) {
        super(ctx, node);
        this.store = new Store(this.config.endpoint);
    }

    onClick(evt) {
        let payload = {
            preset: this.config.name,
            group: this.config.group,
            state: this.state
        };
        this.store.save(payload);
        this.ctx.mediator.trigger('/preset/switched', payload);
    }

    subscribe() {
        this.ctx.mediator.on('/preset/switched', (payload) => {
            if (payload.state
                && payload.preset !== this.config.name
                && payload.group === this.config.group) {
                if (this.state) {
                    this.state = false;
                    this.node.find('input').click();
                }
            }
        });
    }

}