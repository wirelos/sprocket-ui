import $ from 'jquery';
import Slider from './base/Slider/Slider';
//import Store from '../core/store/RestStore';

export default class ParamSlider extends Slider {

    constructor(ctx, node) {
        super(ctx, node);
        //this.store = new Store(this.config.endpoint);
        this.ctx.mediator.on(this.config.topic, (payload) => {
            this.node.find('input').val(payload);
        });
    }
    
    onChange(evt) {
        let msg = JSON.stringify({
            topic: this.config.topic,
            payload: evt.target.value,
            broadcast: 1
        });
        this.ctx.ws.send(msg);
    }
}