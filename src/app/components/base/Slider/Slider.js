import $ from 'jquery';
import Component from '../../../core/Component';
import markup from './Slider.html';

export default class Slider extends Component {

    constructor(ctx, node, template) {
        super(ctx, node, template || markup);
        this.render(this.config);
    }

    onChange(evt) {}

    sliderChange(evt) {
        this.value = evt.target.value;
        this.onChange(evt);
    }


    subscribe() {
        this.node.delegate('input', 'input',
            this.sliderChange.bind(this));
    }

}