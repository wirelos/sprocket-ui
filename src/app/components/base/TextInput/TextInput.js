import $ from 'jquery';
import Component from '../../../core/Component';
import markup from './TextInput.html';

export default class TextInput extends Component {

    constructor(ctx, node, template) {
        super(ctx, node, template || markup);
        this.render(this.config);
    }

    onChange(evt) {}

    inputChange(evt) {
        this.value = evt.target.value;
        this.onInput(evt);
    }


    subscribe() {
        this.node.delegate('input', 'input',
            this.inputChange.bind(this));
    }

}