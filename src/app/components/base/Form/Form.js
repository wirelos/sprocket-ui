import $ from 'jquery';
import Component from '../../../core/Component';
import TextInput from '../TextInput/TextInput';
import Store from '../../../core/store/RestStore';

export default class Form extends Component {

    constructor(ctx, node) {
        super(ctx, node);
        
        $.get(this.config.from, (cfg) => {
            this.node.append(
                $(this.formTpl({
                    endpoint: this.config.endpoint,
                    fields: this.configToFormRow(cfg)
                })).submit(evt => this.ajaxFormhandler(evt))
            );
        });

        //this.node.delegate('click', '.js-submit', this.onSubmit.bind(this));
    }

    formTpl(data) {
        return `
            <form action="${data.endpoint}">
                ${data.fields.join('')}
                <div class="form-row">
                    <button type="submit" class="js-submit">Submit</button>
                </div>
            </form>
        `;    
    } 

    formRowTpl(data) {
        return `
            <div class="form-row wrapped">
                <label for="${data.name}">${data.label}</label>
                <input type="${data.fieldType}" name="${data.name}" id="${data.name}" value="${data.value}"><br>
            </div>
        `;
    }

    configToFormRow (cfg){
        let arr = [];
        for (let key in cfg) {
            arr.push(this.formRowTpl({
                label: key,
                name: key,
                value: cfg[key],
                fieldType: key.indexOf('assword') > -1 ? 'password' : 'text'
            }))
        }
        return arr;
    };
    
    ajaxFormhandler(evt){
        evt.preventDefault();
        let data = $(evt.currentTarget)
            .serializeArray()
            .reduce((obj, item) => {
                obj[item.name] = item.value;
                return obj;
            }, {});
        $.post(this.config.endpoint, { fileName: this.config.filename, config: JSON.stringify(data) })
            .done((data) => {
                alert("data saved");
            })
            .fail((data) => {
                alert("save failed");
            });
    };


    /* onSubmit(evt) {
        let obj = {};

        obj[this.config.name] = this.value;
        //this.store.save(obj);
        console.log(this.value);
    } */

}