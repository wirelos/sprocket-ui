import $ from 'jquery';
import Component from '../../../core/Component';
import markup from './Switch.html';

export default class Switch extends Component {

    constructor(ctx, node, template) {
        super(ctx, node, template || markup);
        this.state = false;
        this.render(this.config);
        this._subscribe();
    }

    onClick(evt) {}

    switchState(evt) {
        this.state = !this.state;
    }

    clickSwitch(evt) {
        this.switchState(evt);
        this.onClick(evt);
    }

    _subscribe() {
        this.node.delegate('.slider', 'click',
            this.clickSwitch.bind(this));
    }

}