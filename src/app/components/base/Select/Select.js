
import $ from 'jquery';
import Component from '../../../core/Component';
import markup from './Select.html';

export default class Select extends Component {

    constructor(ctx, node, template) {
        super(ctx, node, template || markup);
        this.render(this.config);
    }

    onChange(evt) {
        console.log(evt.target.value);
    }

    subscribe() {
        this.node.delegate('select', 'change', this.onChange.bind(this));
    }

}