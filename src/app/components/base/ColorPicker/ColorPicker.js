import $ from 'jquery';
import Component from '../../../core/Component';
import markup from './ColorPicker.html';

export default class ColorPicker extends Component {

    constructor(ctx, node, template) {
        super(ctx, node, template || markup);
        this.render(this.config);
    }

    onChange(evt) {}

    inputChange(evt) {
        this.value = evt.target.value;
        this.onChange(evt);
    }


    subscribe() {
        // change to event input when ws is implemented
        this.node.delegate('input', 'input',
            this.inputChange.bind(this));
    }
    
    /* delegate() {
        return [{
            selector: 'input',
            event: 'input',
            handler: this.inputChange.bind(this)
        }];
    }  */

}