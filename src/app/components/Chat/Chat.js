import $ from 'jquery';
import markup from './Chat.html';
import Component from '../../core/Component';

export default class Chat extends Component {

    constructor(ctx, node, template) {
        super(ctx, node, template || markup);
        this.render(this.config);
        this.ctx.mediator.on(this.config.topic, this.onMessage.bind(this));
        this.ctx.mediator.on("chat/connected", this.connected.bind(this));
        //this.ctx.mediator.on("irc/configValid", this.configValid.bind(this));
        this.node.delegate('input.msg', 'keypress', this.onInput.bind(this));
        this.node.delegate('button.send', 'click', this.send.bind(this));
        this.node.delegate('button.join', 'click', this.join.bind(this));
    }

    templates() {
        return {
            message: (user, msg) => `
                <li>
                    <span class="user-label">${user}</span>
                    <span class="message-text">${msg}</span>
                </li>
            `,
            serverMessage: (msg) => `
                <li>
                    <span class="message-text">${msg}</span>
                </li>
            `
        };
    }

    configValid() {
        this.node.text("Please configure first");
    }

    join(evt) {
        evt.preventDefault();
        let message = JSON.stringify({
            topic: 'irc/join',
            payload: this.sanitizeInput(this.node.find('.channel').val())
        });
        this.ctx.ws.send(message);
        this.node.find('.controls').show();
        //this.node.find('button.join').hide();
    }
    connected() {
        this.node.find('.controls').show();
        this.node.find('button.connect').hide();
    }

    onMessage(msg) {
        let payload = msg.payload; //.replace(/<.+?>/g, '');
        //console.log('onMsg: ' + msg);
        let msgParts = payload.split(':');
        let messages = this.node.find('.messages');
        messages.append(
            msgParts.length == 2 ?
            this.templates().message(msgParts[0], this.sanitizeInput(msgParts[1]))
            : this.templates().serverMessage(this.sanitizeInput(payload))
        );
        this.node.find('.message-container').animate({
            scrollTop: messages[0].scrollHeight
        }, 250);
    }
    sanitizeInput(val) {
        return val.replace(/<.+?>/g, '');
    }
    send(evt) {
        evt.preventDefault();
        let msg = this.node.find('input.msg');
        if (msg.length > 0) {
            let message = JSON.stringify({
                topic: this.sanitizeInput(this.config.topic),
                payload: this.sanitizeInput(msg.val())
            });
            this.ctx.ws.send(message);
            msg.val('');
        }
    }
    onInput(evt) {
        if (evt.keyCode === 13) {
            this.send(evt);
        }
    }

}