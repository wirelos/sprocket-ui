import $ from 'jquery';
import ColorPicker from './base/ColorPicker/ColorPicker';

export default class ParamColor extends ColorPicker {

    constructor(ctx, node) {
        super(ctx, node);
        this.ctx.mediator.on(this.config.topic, (payload) => {
            this.node.find('input').val(payload);
        });
    }

    onChange(evt) {
        this.notify(evt.target.value);
    }

    notify(val){
        let obj = {
            topic: this.config.topic,
            payload: parseInt(val.replace('#', '0x')),
            broadcast: 1
        };
        this.ctx.ws.send(JSON.stringify(obj));
    }

}