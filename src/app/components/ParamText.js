import $ from 'jquery';
import TextInput from './base/TextInput/TextInput';
import Store from '../core/store/RestStore';

export default class ParamText extends TextInput {

    constructor(ctx, node) {
        super(ctx, node);
        this.store = new Store(this.config.endpoint);
    }

    onInput(evt) {
        let obj = {};
        obj[this.config.name] = this.value;
        //this.store.save(obj);
        console.log(this.value);
    }

}