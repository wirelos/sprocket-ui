"use strict";

import $ from 'jquery';
import App from './core/App';
import * as components from './components/exports';

let endpoint = '/ws';

// gradients
// https://uigradients.com

$(() => {
    let app = new App($('body'))
        .components(components)
        .websocket(new WebSocket(endpoint.indexOf('/') === 0 ? "ws://" + window.location.host + endpoint : endpoint));
    app.ws.onmessage = (msg) => {
        app.mediator.trigger('out/chat/log', {topic: 'out/chat/log', payload: msg.data});
    };
    app.mediator.on('pixels/hue', (payload) => {
        let colors = payload.split(',');
        let msg = JSON.stringify({
            topic: 'pixels/state',
            broadcast: 1,
            payload: JSON.stringify({
                //brightness: 64,
                color: parseInt(colors[0].replace('#', '0x'), 16),
                color2: parseInt(colors[1].replace('#', '0x'), 16),
                //totalSteps: 64,
                pattern: 5
            })
        });
        //app.mediator.trigger('pixels/brightness', 64);
        //app.mediator.trigger('pixels/totalSteps', 64);
        app.mediator.trigger('pixels/pattern', 5);
        app.mediator.trigger('pixels/color',    colors[0]);
        app.mediator.trigger('pixels/color2',   colors[1]);
        //console.log('pixels/hue: ' + msg);
        app.ws.send(msg);


    });

    // TODO make components
    $('.collapsible').each((i, container) => {
        container.querySelector('.heading').addEventListener('click', (item) => {
            container.classList.toggle('open');
        });
    });

    $('.js-restart').click((evt) => {
        $.post('/restart');
        alert('restarting...');
    });

    $.get('/heap', (data) => {
        $('.js-heap').html(data);
    });

});

/* let click = element => element.click();
let check = element => element.setAttribute('checked', 'checked');
let uncheck = element => element.removeAttribute('checked');

let endpoint = "http://192.168.1.134";

let switchElementState = element => {
    let state = element.getAttribute('data-state') == 'false';
    element.setAttribute('data-state', state);
    return state;
};
let bindData = (element, attribute, data) => {
    element.setAttribute('data-' + attribute, data);
};
let leadingZero = (time) => {
    return (time < 10 ? "0" : "") + time;
};
let setMode = (mode) => {
    var formData = new FormData();
    formData.append("preset", mode);
    Sui.http.ajax({
        endpoint: endpoint + '/matrix/mode',
        method: 'POST',
        data: formData
    });
};

let setScrollText = (txt) => {
    var formData = new FormData();
    formData.append("scrollText", txt);
    Sui.http.ajax({
        endpoint: endpoint + '/matrix/text',
        method: 'POST',
        data: formData
    });
};


Sui.ready(() => {
    let debugResponse = (data) => {
        document.querySelector('#response').innerHTML = data;
    };

    Sui.http.ajax({
        endpoint: '/wifiConfig',
        method: 'GET'
    }, (data) => {
        Sui.select('[name="ssid"]').forEach((el) => {
            el.value = data;
        });
    });

    // init collapsible containers
    Sui.select('.collapsible').forEach((container) => {
        container.querySelector('.heading').addEventListener('click', (item) => {
            container.classList.toggle('open');
        });
    });

    let switchState = (evt) => {      //evt.preventDefault();
        let state = !(evt.target.getAttribute('data-state') == 'true');
        evt.target.setAttribute('data-state', state);
        setMode(el.getAttribute('name'));
    };

    let elClick = (el) => {
        el.addEventListener('click', switchState);
        return el;
    };
    Sui.select('.switch.demo').forEach(elClick);

    setInterval(() => {
        let time = new Date();
        let txt = leadingZero(time.getHours()) + ':' + leadingZero(time.getMinutes());
        var formData = new FormData();
        formData.append("staticText", txt);
        Sui.http.ajax({
            endpoint: endpoint + '/matrix/text',
            method: 'POST',
            data: formData
        });
    }, 5000);

}); */