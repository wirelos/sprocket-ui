FROM node:latest

# Create app directory
RUN mkdir -p /usr/app
WORKDIR /usr/app
VOLUME /usr/app/config

# Install app
COPY . /usr/app
RUN yarn install
RUN yarn build:frontend

EXPOSE 8080
#CMD [ "yarn", "app" ]
